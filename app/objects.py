# standard library

import shelve
from dataclasses import dataclass

__all__ = ['database']


class ORM(object):
    def __init__(self, db_name):
        self.db = db_name
        self.path = f'data/{db_name}'
        with shelve.open(self.path) as db:
            db.setdefault(self.db, {})

    def read(self, key=None):
        with shelve.open(self.path) as db:
            if key:
                return db[self.db].get(key)
            return db[self.db]

    def write(self, data):
        with shelve.open(self.path) as db:
            d = db[self.db]
            d.update(data)
            db[self.db] = d

    def remove(self, key):
        with shelve.open(self.path) as db:
            data = db[self.db]
            data.pop(key)
            db[self.db] = data


@dataclass
class database:
    proxies: object=ORM('proxies.db')
    profiles: object=ORM('profiles.db')


