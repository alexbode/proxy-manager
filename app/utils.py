# standard library
from functools import wraps

# quart
from quart import request, jsonify

# local imports
from .objects import database

__all__ = ['format_proxy', 'requires_auth']
db = database()


def format_proxy(proxy: dict, format: str=None):
    if format == 'selenium':
        return {
            "httpProxy": proxy['ip'],
            "ftpProxy": proxy['ip'],
            "sslProxy": proxy['ip'],
            "proxyType": "MANUAL"
        }
    elif format == 'requests':
        return {
          'http': f'http://{proxy}',
          'https': f'http://{proxy}',
        }
    else:
        return proxy


def check_auth(username:str, password:str):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'admin' and password == 'secret'


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return jsonify({
        'status': 'failed',
        'message': 'authenticatin failed'
        }), 401


def requires_auth(methods: list):
    def require_auth(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if request.method in methods:
                auth = request.authorization
                if not auth or not check_auth(auth.username, auth.password):
                    return authenticate()
            return f(*args, **kwargs)
        return decorated
    return require_auth
