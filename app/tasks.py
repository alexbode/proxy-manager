# standard library
import os
import asyncio

# logging
from loguru import logger

# utils
import aiohttp

# local imports
from .objects import database

__all__ = ['run_update_proxy_list', 'update_proxy_list']
db = database()


async def run_update_proxy_list(t=600):
    """ Updates the proxy list every 10 mins
    """
    while True:
        try:
            async with aiohttp.ClientSession() as session:
                await update_proxy_list(session)
        except Exception as e:
            logger.exception(e)
        else:
            logger.debug('[ok] updated proxy list')
        finally:
            await asyncio.sleep(t)


async def update_proxy_list(session):
    saved_proxy_list = db.proxies.read('proxy_list')
    if os.getenv('APP_ENV', 'local') != 'local' and saved_proxy_list:
        validated_proxy = await asyncio.gather(*[check_proxy_health(proxy, session) for proxy in saved_proxy_list])
        logger.debug(f'[ok] validated proxies {validated_proxy}')
        db.proxies.write({'proxy_list': validated_proxy})
        return validated_proxy
    return saved_proxy_list


async def check_proxy_health(proxy, session):
    try:
        async with session.get('http://ifconfig.co/json',
                proxy=f'http://{proxy["ip"]}', timeout=5) as response:
            ifconfig = await response.json()
            logger.debug(f'[ok] proxy health good: {proxy}')
            port = proxy["ip"].split(':')[1]
            proxy['ip'] = ifconfig['ip']+':'+port
            proxy['health'] = 'good'
    except:
        logger.exception('[x] Tasks')
        proxy['health'] = 'bad'
    finally:
        return proxy
