# standar library
import asyncio
import json
from random import choice, shuffle

# quart
from quart import Quart, jsonify, request

# utils
import aiohttp
from loguru import logger

# application imports
from .objects import database
from .utils import *
from .tasks import *

## todo:
## pytest
## db endpoint?
## push to github
## CircleCI

app = Quart(__name__)
app.add_url_rule('/healthcheck', 'healthcheck', lambda: ('', 200))
db = database()


@app.before_serving
async def initialize_tasks():
    """ Start task that updates and checks proxies health
        run every 10 mins by default
    """

    asyncio.ensure_future(run_update_proxy_list())


@app.errorhandler(Exception)
def internal_error(error):
    logger.exception('Caught Exception')
    return jsonify({'status': 'failed', 'message': 'internal server error'}), 500


@app.route('/')
async def hello():
    await asyncio.sleep(1)
    return 'Its working!'


@app.route('/force_update_proxy_list', methods=['GET'])
async def force_update():
    """ Force upda the proxy list and test all proxies health
    """

    async with aiohttp.ClientSession() as session:
        validated_proxy_list = await asyncio.ensure_future(update_proxy_list(session))
    return jsonify({'proxy_list': validated_proxy_list})


@app.route('/proxies', methods=['GET'])
async def proxies():
    """ returns list of proxies in pool
    """

    proxy_list = db.proxies.read()
    return jsonify(proxy_list)


@app.route('/proxy', methods=['GET', 'PUT', 'DELETE'])
@requires_auth(['PUT', 'DELETE'])
async def random_proxy():
    """ returns randomly selected proxy or manually add
        a new one to the pool or delete from pool
    """

    proxy_format = request.args.get('format')
    if request.method == 'GET':
        proxy_list = db.proxies.read('proxy_list')
        if not proxy_list or not [proxy for proxy in proxy_list if proxy['health'] == 'good']:
            return jsonify({'status': 'failed', 'message': 'no healthy proxies in pool'})
        proxy = choice(list(filter(lambda x: x['health']=='good', proxy_list)))
        return jsonify(format_proxy(proxy, proxy_format))
    if request.method == 'PUT':
        json_body = await request.get_data()
        try:
            data = json.loads(json_body.decode('utf-8'))
            if not data.get('ip'):
                return jsonify({'status': 'failed', 'message': 'must include key "ip" in json'})
            if not data.get('provider'):
                return jsonify({'status': 'failed', 'message': 'must include "provider" key in json ex. aws or digital ocean'})
            if ':' not in data['ip']:
                return jsonify({'status': 'failed', 'message': 'please specify port'})
        except json.decoder.JSONDecodeError:
            return jsonify({'status': 'failed', 'message': 'posted data must be json'})
        proxy_list = db.proxies.read('proxy_list')
        if proxy_list:
            if data['ip'] in [proxy['ip'] for proxy in proxy_list]:
                return jsonify({'status': 'failed', 'message': 'ip already in use'})
        else:
            proxy_list = []
        proxy = {'ip': data['ip'], 'provider': data['provider'], 'health': None}
        proxy_list.append(proxy)
        db.proxies.write({'proxy_list': proxy_list})
        return jsonify({'status': 'success', 'message': 'proxy added to pool'})
    if request.method == 'DELETE':
        json_body = await request.get_data()
        proxy_list = db.proxies.read('proxy_list')
        try:
            data = json.loads(json_body.decode('utf-8'))
            if not data.get('ip'):
                return jsonify({'status': 'failed', 'message': 'must include key "ip" in json'})
            logger.debug(f'{proxy_list}, {data["ip"]}')
            if data['ip'] not in [proxy['ip'] for proxy in proxy_list]:
                return jsonify({'status': 'failed', 'message': 'proxy not found'})
        except json.decoder.JSONDecodeError:
            return jsonify({'status': 'failed', 'message': 'posted data must be json'})
        [proxy_list.remove(proxy) for proxy in proxy_list if proxy['ip'] == data['ip']]
        db.proxies.write({'proxy_list': proxy_list})
        return jsonify({'status': 'success', 'message': 'proxy removed from pool'})


@app.route('/<profile_name>/proxy/new', methods=['GET'])
async def new_proxy(profile_name):
    """ Cycle to new proxy, does not blacklist previous
        proxy address like DELETE /<profile>/proxy
        new profile create if not found
    """

    proxy_format = request.args.get('format')
    # create new profile
    if not db.profiles.read(profile_name):
        response = await random_proxy()
        proxy = await response.get_json()
        db.profiles.write({profile_name: {'current_proxy': proxy, 'blacklist': []}})
        logger.debug('[+] created new profile')
        return jsonify(format_proxy(proxy, proxy_format))
    # get new proxy
    profile = db.profiles.read(profile_name)
    old_proxy = profile['current_proxy']
    proxy_list = db.proxies.read('proxy_list')
    blacklist = profile['blacklist']
    shuffle(proxy_list)
    for proxy in proxy_list:
        if proxy['ip'] != old_proxy['ip'] and proxy['ip'] not in [proxy['ip'] for proxy in blacklist]:
            profile.update({'current_proxy': proxy, 'blacklist': blacklist})
            db.profiles.write({profile_name: profile})
            return jsonify(format_proxy(proxy, proxy_format))
    profile.update({'current_proxy': {'ip': None}})
    db.profiles.write({profile_name: profile})
    return jsonify({'status': 'failed', 'message': 'no proxies left in pool'})


@app.route('/<profile_name>/proxy', methods=['GET', 'DELETE'])
async def proxy(profile_name):
    """ Returns current proxy address associated
        associated with specified profile or deletes
        it and returns a new one
        new profile created if not found
    """

    proxy_format = request.args.get('format')
    if request.method == 'GET':
        # create new profile
        if not db.profiles.read(profile_name):
            response = await random_proxy()
            proxy = await response.get_json()
            db.profiles.write({profile_name: {'current_proxy': proxy, 'blacklist': []}})
            logger.debug('[+] created new profile')
            return jsonify(format_proxy(proxy, proxy_format))
        else:
            proxy = db.profiles.read(profile_name)['current_proxy']
            return jsonify(format_proxy(proxy, proxy_format))
    if request.method == 'DELETE':
        if not db.profiles.read(profile_name):
            return jsonify({
                'status': 'failed',
                'message': 'profile does not exist'
                })
        else:
            profile = db.profiles.read(profile_name)
            old_proxy = profile['current_proxy']
            blacklist = profile['blacklist']
            blacklist.append(old_proxy)
            for proxy in db.proxies.read('proxy_list'):
                if proxy not in profile.get('blacklist') and proxy != old_proxy:
                    profile.update({'current_proxy': proxy, 'blacklist': blacklist})
                    db.profiles.write({profile_name: profile})
                    logger.debug(f'[x] blacklisted proxy: {profile_name} {proxy["ip"]}')
                    return jsonify(proxy)
            profile.update({'current_proxy': {'ip': None}})
            db.profiles.write({profile_name: profile})
            return jsonify({'status': 'failed', 'message': 'no proxies left in pool'})


@app.route('/profiles', methods=['GET'])
async def profiles():
    """ Returns all profiles
    """

    profiles = db.profiles.read()
    return jsonify(profiles)


@app.route('/<profile_name>/profile', methods=['GET', 'DELETE'])
@requires_auth(['DELETE'])
async def profile(profile_name):
    """ Returns profile config or deletes it
    """

    if not db.profiles.read(profile_name):
        return jsonify({
            'status': 'failed',
            'message': 'profile does not exist'
            })
    if request.method == 'GET':
        response = db.profiles.read(profile_name)
        response.update({'status': 'success'})
        return jsonify(response)
    if request.method == 'DELETE':
        db.profiles.remove(profile_name)
        logger.debug(f'[x] deleted profile {profile_name}')
        return jsonify({'status': 'success'})
