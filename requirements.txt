-i https://pypi.org/simple
aiodns==2.0.0
aiofiles==0.4.0
aiohttp==3.5.4
ansimarkup==1.4.0
async-timeout==3.0.1
atomicwrites==1.3.0
attrs==19.1.0
better-exceptions-fork==0.2.1.post6
blinker==1.4
cchardet==2.1.4
cffi==1.12.3
chardet==3.0.4
click==7.0
colorama==0.4.1
h11==0.9.0
h2==3.1.0
hpack==3.0.0
hypercorn==0.6.0
hyperframe==5.2.0
idna==2.8
importlib-metadata==0.17
itsdangerous==1.1.0
jinja2==2.10.1
loguru==0.2.5
markupsafe==1.1.1
more-itertools==7.0.0 ; python_version > '2.7'
multidict==4.5.2
packaging==19.0
pluggy==0.12.0
py==1.8.0
pycares==3.0.0
pycparser==2.19
pygments==2.4.2
pyparsing==2.4.0
pytest-asyncio==0.10.0
pytest==4.6.2
pytoml==0.1.20
quart==0.9.1
six==1.12.0
sortedcontainers==2.1.0
typing-extensions==3.7.2
wcwidth==0.1.7
wsproto==0.14.1
yarl==1.3.0
zipp==0.5.1
