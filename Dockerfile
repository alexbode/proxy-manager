FROM python:3.7-alpine as BASE
RUN apk add --no-cache libffi-dev musl-dev g++
COPY . /app
RUN pip3 install --no-deps -r /app/requirements.txt

FROM python:3.7-alpine
COPY --from=BASE /usr/local/lib/python3.7/site-packages /usr/local/lib/python3.7/site-packages
COPY --from=BASE /app /app 
COPY --from=BASE /usr/local/bin/hypercorn /usr/local/bin/hypercorn
WORKDIR /app
CMD hypercorn -w 1 --access-log - -b :8080 app.main:app
