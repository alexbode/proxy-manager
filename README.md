# Proxy Manager
Proxy Manager


### Quickstart
The proxy manager supports 2 methods of managing client side proxies, Stateless and Stateful
To initiate the proxy manager proxies will need to be added manually with PUT /proxy

##### Stateless 
To simply receive a random proxy address from the proxy pool you can simply GET /proxy
note: since it is chosen randomly repetition is possible
You can get a list of all the proxies by GET /proxies

##### Stateful
You can create a profile that is saved server side by GET /<profile_name>/proxy/new\n
for example if you want to create a new profile for account aggregation for chime bank\n
you can GET /bl-chime/proxy/new and a proxy address will be returned which will be saved to the bl-chime\n
profile. If you GET /bl-chime/proxy you will receive the same proxy address as the revious. If you get \n
blocked and need a new proxy for bl-chime you can GET /bl-chime/proxy/new.\n



### ToDo
* format response for selenium or requests


## Table of Contents
* [API Endpoints](#api-endpoints)
  * [/proxy](#proxy)
  * [/proxies](#proxies)
  * [/profiles](#profiles)
    * [<profile_name>/proxy](#get-bankpull)
    * [<profile_name>/proxy/new](#add-bankpull)
    * [<profile_name>/proxy/profile](#add-bankpull)
